import hashlib
from djongo import models as mongo_models
from django.db import models
from django import forms
from django.utils import timezone


NOW = timezone.now()


class Order(mongo_models.Model):
    p_date = models.DateField(default=NOW)
    product = mongo_models.CharField(max_length=250)
    class Meta:
        abstract = True

class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = '__all__'
 
class Customer(mongo_models.Model):
    name = mongo_models.CharField(max_length=250)
    surname = mongo_models.CharField(max_length=250)
    bdate = mongo_models.DateField()
    r_date = mongo_models.DateField()
    hf = mongo_models.CharField(max_length=100, unique=True)
    order = mongo_models.EmbeddedModelField(\
            model_container=Order,\
            model_form_class=OrderForm,\
            null=True
            )
    objects = mongo_models.DjongoManager()

    def save(self, *args, **kwargs):
        h = u"%s %s %s"%(self.name, self.surname, self.bdate)
        if not self.hf:
            self.hf = hashlib.sha224(h.encode()).hexdigest()
        return super().save(*args, **kwargs)


# Create your models here.
