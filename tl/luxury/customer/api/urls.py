from django.urls import path
from .views import *
from customer.models import *

urlpatterns = [
    path('users/', CustListAPI.as_view(), name="users-all"),
    path('users/<str:data>/', CustByDateAPI.as_view(), name="users-by-date")
]

