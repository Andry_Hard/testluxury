from rest_framework import generics
from .serializers import CustomerSerializer
from customer.models import *



class CustListAPI(generics.ListAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer


class CustByDateAPI(generics.ListAPIView):
    serializer_class = CustomerSerializer

    def get_queryset(self):
        qs = Customer.objects.filter(r_date=self.kwargs['data'])
        return qs
