from rest_framework import serializers
from customer.models import Customer


class CustomerSerializer(serializers.ModelSerializer):
    order = serializers.SerializerMethodField()
    class Meta:
        model = Customer
        fields = '__all__'

    def get_order(self, obj):
        return {'p_data': obj.order.p_date, 'product': obj.order.product}
