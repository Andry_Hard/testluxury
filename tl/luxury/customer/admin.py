import csv
import codecs
import hashlib
from django.contrib import admin
from .models import Customer, Order
from django import forms
from django.urls import path, include
from django.shortcuts import render, redirect
from django.utils import timezone


NOW=timezone.now()


class CsvImportForm(forms.Form):
    csv_file = forms.FileField()


class ImportCsvMixin:
    def import_from_csv(self, request):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]
        response = HttpResponse(content_type='text/csv')
        reader = csv.reader()


class CustomerImport(admin.ModelAdmin):
    change_list_template = "customer/cust_changelist.html"

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('import-csv/', self.import_csv),
        ]
        return my_urls + urls

    def import_csv(self, request):
        if request.method == "POST":
            csv_file = request.FILES["csv_file"]
            reader = csv.DictReader(codecs.iterdecode(csv_file, 'utf-8'), delimiter=',')
            for row in reader:
                name = row['FirstName']
                surname = row['LastName']
                bdate = row['BirthDate'].replace("/", "-")
                r_date = row["RegistrationDate"].replace("/", "-")
                h = u"%s %s %s"%(name, surname, bdate)
                t_h = hashlib.sha224(h.encode()).hexdigest()
                try:
                    obj = Customer.objects.get(hf=t_h)
                    obj.name=name
                    obj.surname=obj.surname
                    obj.bdate=bdate
                    obj.r_date=r_date
                except:
                    obj = Customer(
                        hf=t_h,
                        name=name,
                        surname=surname,
                        bdate=bdate,
                        r_date=r_date,
                        order = Order(product="test")
                    )
                obj.save()
            self.message_user(request, "Your csv file has been imported")
            return redirect("..")
        form = CsvImportForm()
        payload = {"form": form}
        return render(request, "customer/csv_form.html", payload)
    


admin.site.register(Customer, CustomerImport)

# Register your models here.
