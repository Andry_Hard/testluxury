import csv
from django import forms
from django.db import models


class FileImportForm(forms.Form):
    csv_file = models.FileField()
   
