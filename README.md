Проект Django + MongoDB 

Додатково реалізовано завантаження з csv-файлу

Для запуску проекту необхідно :

    python>=3.6
    django2
    djongo

або середовище з налаштованим docker

Запуск :

    docker-compose build
    docker-compose up
    docker-compose run web python3 luxury/manage.py makemigrations
    docker-compose run web python3 luxury/manage.py migrate
    docker-compose run web python3 django-admin createsuperuser
    docker-compose up


Заходимо в адмінку через стандартний урл
